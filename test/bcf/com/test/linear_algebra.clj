(ns com.bcf.test.linear-algebra
  (:require[clojure.test :refer [is deftest]]
           [com.bcf.linear-algebra :refer [square-mat id-mat
                                           mat-eq]]))

(deftest square-mat-test
  (is (= (square-mat  3 1) [[1 1 1] [1 1 1] [1 1 1]])))

(deftest id-mat-test
  (is (= (id-mat  3) [[1 0 0] [0 1 0] [0 0 1]])))

(deftest mat-eq-test
  (is (= [[1 2 3] [4 5 6]] [[1 2 3] [4 5 6]]))
  (is (= [[1 2 3] [4 5 6]] [[0 0 0] [0 0 0]]))
  (is (mat-eq [[1 2 3] [4 5 6]] [[1 2 3] [4 5 6]]))
  (is (mat-eq [[1 2 3] [4 5 6]] [[0 0 0] [0 0 0]])))

(comment
  (square-mat-test)
  (id-mat-test)
)
