(ns com.bcf.linear-algebra
  (:require [clojure.pprint :refer [pprint]]
            [clojure.core.matrix :refer [matrix pm matrix? row-count column-count]]
            [clojure.core.matrix :as M]
            [clatrix.core :as cl]
            [incanter.charts :refer [xy-plot add-points]]
            [incanter.core :refer [view]]))

(defn system [] {:state :inactive})
(defn start [system] {:state :runningp})
(defn stop [system] {:state :stopped})

(defn square-mat
  [n e & {:keys [implementation]
          :or {implementation :persistent-vector}}]
  (let [repeater #(repeat n %)]
    (matrix implementation (-> e repeater repeater))))

(defn id-mat
  [n]
  (let [init (square-mat  n 0 :implementation :clatrix)
        identity-fn (fn [i j n] (if (= i j) 1 n))]
    (cl/map-indexed identity-fn init)))

(defn rand-square-clmat
  [n]
  (cl/map rand-int (square-mat n 100 :implementation :clatrix)))

(defn rand-square-mat
  [n]
  (matrix  (repeatedly n #(map rand-int (repeat n 100)))))

(defn mat-eq
  [A B]
  (and (= (count A) (count B))
       (reduce #(and %1 %2) (map = A B))))
