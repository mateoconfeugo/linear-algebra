(defproject com.bcf/linear-algebra "0.1.0"
  :description "Linear Algebra and Computer learning experiments"
  :url "https://bitbucket.org/mateoconfeugo/linear-algebra"
  :license {:name "TODO: Choose a license" :url "http://choosealicense.com/"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [net.mikera/core.matrix "0.20.0"]
                 [clatrix "0.3.0"]
                 [incanter "1.5.6"]]
  :profiles {:dev {:dependencies [[org.clojure/tools.namespace "0.2.11"]
                                  [alembic "0.3.2"]]
                   :source-paths ["dev"]
                   :plugins [[cider/cider-nrepl "0.9.1"]
                             [refactor-nrepl "1.1.0"]]}})
